<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.List,java.util.Comparator" contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'/>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'/>
    <title>Территориальная избирательная комиссия ${Region}</title>
  </head>
  <body class='container'>
    <%@include file='/WEB-INF/jspf/topmenu.jspf' %>
    <img src='/header.jpg?rand=<%=(new java.util.Date()).getTime() %>' width='100%' style='max-height:150px;'/>
    <div class='container-fluid'>
      <div class='row' style='margin-top: 20px;'>
        <div class='col-sm-5 col-lg-3'>
          <%@include file='/WEB-INF/jspf/sidemenu.jspf' %>
          <%@include file='/WEB-INF/jspf/banners.jspf' %>
        </div>
        <div class='col-sm-7 col-lg-9' style='padding-right:0px;'>
          <!--Content--><% if(request.getParameter("laws")==null) { %>
          <div class='panel panel-default'>
            <div class='list-group panel-body' style='padding:0px;'>
              <a href='?laws=1' class='list-group-item'><b>Постановления избирательной комиссии</b></a>
            </div>
          </div><% } %>
          <div class='panel panel-default'>
            <div class='panel-heading' style='text-align:center'><b><%=(request.getParameter("laws")==null ? "Документы" : "Постановления")%> избирательной комиссии</b></div>
            <% try { List<String> doc = (List<String>)request.getAttribute("DocsList");
            if(doc.size() > 0) {
              out.println("<div class='list-group panel-body' style='padding:0px;'>");
              for(int i = 0; i < doc.size(); ++i) {
                String name = doc.get(i).replace("_", "/");
                name = name.substring(0, name.lastIndexOf("."));
                if(request.getParameter("laws")!=null && name.startsWith("№"))
                  out.println("<a href='/getDoc?id=".concat(String.valueOf(i)).concat("' class='list-group-item'>").concat(name).concat("</a>"));
                else if(request.getParameter("laws")==null && !name.startsWith("№"))
                  out.println("<a href='/getDoc?id=".concat(String.valueOf(i)).concat("' class='list-group-item'>").concat(name).concat("</a>"));
              }
            } else { %>
              <div class='panel-body'><p align='center'>Нет документов</p>
            <% } }catch(NullPointerException ex) { out.println("<div class='panel-body'><p align='center'>Нет документов</p>");} %>
            </div>
          </div>
          <!--/Content-->
        </div>
      </div>
    </div>
    <%@include file='/WEB-INF/jspf/footer.jspf' %>
  </body>
</html>