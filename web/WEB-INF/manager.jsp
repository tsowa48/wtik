<%@page import="tik.news"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='viewport' content='width=device-width, initial-scale=1'/>
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'/>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <script src='https://cdn.ckeditor.com/4.7.1/full/ckeditor.js'></script>
    <title>Администратор сайта ТИК ${Region}</title>
  </head>
  <body class='container'>
    <ul class='nav nav-tabs'>
      <li class='active'><a href='#index'>Главная</a></li>
      <li><a href='#voters'>Численность избирателей</a></li>
      <li><a href='#news'>Новости</a></li>
      <li><a href='#docs'>Документы избирательной комиссии</a></li>
      <li><a href='#teach'>Обучение</a></li>
      <li><a href='#reception'>Приемная избирательной комиссии</a></li>
      <!--li><a href='/logout'>Выход</a-->
    </ul>

    <div class='tab-content'>
      <div class='tab-pane fade active in' id='index'>
        <form class='panel panel-default' action='#' method='post' enctype='multipart/form-data'>
          <input type='hidden' name='action' value='index'/>
          <div class='panel-body'>
            <div class='input-group'>
              <span class='input-group-addon'>Картинка шапки (jpg)</span>
              <input type='file' class='form-control' name='header' accept='.jpg'/>
            </div>
            <br>
            <div class='input-group'>
              <span class='input-group-addon'>Фото председателя (jpg)</span>
              <input type='file' class='form-control' name='lico' accept='.jpg'/>
            </div>
            <br>
            <div class='input-group'>
              <span class='input-group-addon'>Обращение председателя</span>
              <textarea class='form-control' rows='10' name='obrash' required>${Obrash}</textarea>
            </div>
            <br>
            <div class='input-group'>
              <span class='input-group-addon'>Ф.И.О. председателя</span>
              <input type='text' class='form-control' name='pred' value='${Pred}' required/>
            </div>
          </div>
          <div class='panel-footer'>
            <input type='submit' class='btn btn-success' value='Сохранить изменения'/>
          </div>
        </form>
      </div>
      <div class='tab-pane fade' id='voters'>
        <form class='panel panel-default' action='#' method='post' enctype='multipart/form-data'>
          <input type='hidden' name='action' value='voters'/>
          <div class='panel-body'>
            <div class='input-group'>
              <span class='input-group-addon'>Численность избирателей (csv)</span>
              <input name='voters' type='file' class='form-control' accept='.csv'/>
            </div>
            </div>
          <div class='panel-footer'>
            <button type='submit' class='btn btn-success'>Сохранить изменения</button>
          </div>
        </form>
      </div>
      <div class='tab-pane fade' id='news'>
      
        <div class='panel panel-default'>
          <div class='panel-body list-group' style='padding:0px;'>
          <% try {
              List<news> News = (List<news>)request.getAttribute("NewsList");
              for(int i = 0; i < News.size(); ++i) {
                String name = "".concat(News.get(i).getHeader());
                out.print("<a href='#' class='list-group-item' data-target='#editNewsModal' data-toggle='modal' data-id='".concat(String.valueOf(News.get(i).getID())).concat("'>"));
                out.println(name.concat("</a>"));
            } } catch(NullPointerException ex) {} %>
          </div>
          <div class='panel-footer'><button type='button' class='btn btn-success' data-toggle='modal' data-target='#addNewsModal'>Добавить новость</button></div>
        </div>
        <!--Modal ADD NEWS-->
        <div id='addNewsModal' class='modal fade' role='dialog'>
          <div class='modal-dialog modal-lg'>
            <form class='modal-content' action='#' method='post' enctype='multipart/form-data'>
              <input type='hidden' name='action' value='addnews'/>
              <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                <h4 class='modal-title'>Новость</h4>
              </div>
              <div class='modal-body'>
                <div class='input-group'>
                  <span class='input-group-addon'>Название:</span>
                  <input class='form-control' type='text' name='newsHeader' required/>
                </div><br>
                <div class='input-group'>
                  <span class='input-group-addon'>Анонс:</span>
                  <input class='form-control' type='text' name='newsAnnounce' required/>
                </div><br>
                <div class='input-group'>
                  <span class='input-group-addon'>Картинка анонса:</span>
                  <input class='form-control' type='file' name='newsImage' accept='.jpg'/>
                </div><br>
                <div class='item-group'>
                  <label for='newsContent'>Содержание:</label>
                  <textarea class='form-control' rows='20' id='newsContent' name='newsContent' required></textarea>
                </div>
              </div>
              <div class='modal-footer'>
                <button type='submit' class='btn btn-success'>Добавить</button>
              </div>
            </form>
          </div>
        </div>
        <!--/Modal-->
        
        <!--Modal EDIT NEWS-->
        <div id='editNewsModal' class='modal fade' role='dialog'>
          <div class='modal-dialog modal-lg'>
            <form class='modal-content' action='#' method='post' enctype='multipart/form-data'>
              <input type='hidden' name='action' value='editnews'/>
              <input type='hidden' name='news_id' id='news_id' />
              <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                <h4 class='modal-title'>Новость</h4>
              </div>
              <div class='modal-body' id='editNewsBody'>
                
              </div>
              <div class='modal-footer'>
                <button type='button' class='btn btn-danger' data-dismiss='modal' onclick="removeCurrentNews($('#news_id').val());">Удалить</button>
                <button type='submit' class='btn btn-success'>Редактировать</button>
              </div>
            </form>
          </div>
        </div>
        <!--/Modal-->
        
      </div>
      <div class='tab-pane fade' id='docs'>
      
        <div class='panel panel-danger'>
          <div class='panel-heading'><b>Документ, выбранный из списка, будет удален</b></div>
          <div class='panel-body list-group' style='padding:0px;'>
            <% try {
              List<String> doc = (List<String>)request.getAttribute("DocsList");
              for(int i = 0; i < doc.size(); ++i) {
                out.print("<a href='?action=delDoc&id=".concat(String.valueOf(i)).concat("' class='list-group-item'>"));
                String name = doc.get(i).replace("_", "/");
                name = name.substring(0, name.lastIndexOf("."));
                out.println(name.concat("</a>"));
            } } catch(NullPointerException ex) {} %>
          </div>
          <form class='panel-footer form-inline' action='#' method='post' enctype='multipart/form-data'>
            <input type='hidden' name='action' value='adddocs'/>
            <div class='input-group'>
              <span class='input-group-addon'>Новый документ</span>
              <input name='doc' type='file' class='form-control' accept='.doc, .docx, .xls, .xlsx, .rtf, .txt, .ppt, .pptx'/>
            </div>
            <button type='submit' class='btn btn-success'>Загрузить документ</button>
          </form>
        </div>
        
      </div>
      <div class='tab-pane fade' id='teach'>
        <form class='panel panel-default' action='#' method='post' enctype='multipart/form-data'>
          <input type='hidden' name='action' value='teach'/>
          <div class='panel-body'>
            <div class='input-group'>
              <span class='input-group-addon'>План обучения (csv)</span>
              <input name='teach' type='file' class='form-control' accept='.csv'/>
            </div>
          </div>
          <div class='panel-footer'>
            <button type='submit' class='btn btn-success'>Сохранить изменения</button>
          </div>
        </form>
      </div>
      <div class='tab-pane fade' id='reception'>
        <form class='panel panel-default' action='#' method='post' enctype='multipart/form-data'>
          <input type='hidden' name='action' value='reception'/>
          <div class='panel-body'>
            <div class='input-group'>
              <span class='input-group-addon'>Адрес</span>
              <input name='address' type='text' class='form-control' required value='${Address}'/>
            </div>
            <br>
            <div class='input-group'>
              <span class='input-group-addon'>Телефон</span>
              <input name='phone' type='tel' class='form-control' required value='${Phone}'/>
            </div>
            <br>
            <div class='input-group'>
              <span class='input-group-addon'>Факс</span>
              <input name='fax' type='text' class='form-control' required value='${Fax}'/>
            </div>
            <br>
            <div class='input-group'>
              <span class='input-group-addon'>Электронная почта</span>
              <input name='email' type='email' class='form-control' value='${Email}'/>
            </div>
            <br>
            <div class='input-group'>
              <span class='input-group-addon'>График работы</span>
              <textarea name='grafik' class='form-control' rows='6'>${Grafik}</textarea>
            </div>
          </div>
          <div class='panel-footer'>
            <button type='submit' class='btn btn-success'>Сохранить изменения</button>
          </div>
        </form>
      </div>
    </div>

    <script type='text/javascript'>
    function removeCurrentNews(nid) {
      $.ajax({ type: 'GET', url: '/removeCurrentNews', data: 'nid='+nid});
      window.location.reload(false);
    }
    
    $(function(){
CKEDITOR.replace('newsContent', {
  toolbarGroups: [
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		'/',
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
			],
      removeButtons: 'Source,Save,NewPage,Preview,Print,Templates,Find,Replace,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,Blockquote,BidiRtl,BidiLtr,Language,Anchor,Flash,Smiley,SpecialChar,PageBreak,Iframe,About,ShowBlocks,Maximize,Styles,Format,Redo,Undo',
      removePlugins: 'elementspath'
});
$(document).ready(function(){
$(".nav-tabs a").click(function(){$(this).tab('show');});


$('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
    var data_id = '';
    if (typeof $(this).data('id') !== 'undefined') {
      data_id = $(this).data('id');
      $.ajax({ type: 'GET', url: '/getNewsModal', data: 'nid='+data_id, success: function(data) { $('#editNewsBody').html(data); } });
    }
    $('#news_id').val(data_id);
  });

$.fn.modal.Constructor.prototype.enforceFocus=function(){modal_this = this;$(document).on('focusin.modal',function(e){if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {modal_this.$element.focus();}});};});});

    </script>
  </body>
</html>
