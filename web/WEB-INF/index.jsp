<%@page import="java.util.List"%>
<%@page import="tik.news"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'/>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'/>
    <title>Территориальная избирательная комиссия ${Region}</title>
  </head>
  <body class='container'>
    <%@include file='/WEB-INF/jspf/topmenu.jspf' %>
    <img src='/header.jpg?rand=<%=(new java.util.Date()).getTime() %>' width='100%' style='max-height:150px;'/>
    <div class='container-fluid'>
      <div class='row' style='margin-top: 20px;'>
        <div class='col-sm-5 col-lg-3'>
          <%@include file='/WEB-INF/jspf/sidemenu.jspf' %>
          <%@include file='/WEB-INF/jspf/banners.jspf' %>
        </div>
        <div class='col-sm-7 col-lg-9' style='padding-right:0px;'>
          <!--Content-->
          <div class='panel panel-default'>
            <div class='panel-heading' align='center'><b>Обращение председателя комиссии</b></div>
            <div class='panel-body'>
              <p align='center'><img src='/lico.jpg?rand=<%=(new java.util.Date()).getTime() %>' style='max-width:200px;max-height:200px;'/></p>
              <p align='justify'>${Pred_Welcome}</p><br>
              <p align='right'><i>С уважением,<br>Председатель территориальной избирательной комиссии<br>${Region} ${Pred_FIO}</i></p>
            </div>
          </div><% try { List<news> News = (List<news>)request.getAttribute("NewsList"); %>
          <div class='panel-group'>
            <%
              for(int i = 0; i < (News.size() > 5 ? 5 : News.size()); ++i) {
                if(News.get(i) == null) continue;
            %>
            <div class='panel panel-default'>
              <div class='panel-body'>
                <img src='/pic.jpg?id=<%=News.get(i).getID() %>' style='max-width:150px;max-height:150px;float:left;margin:5px'/>
                <h4><%=News.get(i).getHeader() %></h4>
                <p align='justify'><%=News.get(i).getAnnounce() %></p>
              </div>
              <div class='panel-footer'>Опубликовано: <%=News.get(i).getDate() %> <a href='/news?id=<%=News.get(i).getID() %>' style='float:right;'>Подробнее</a></div>
            </div>
          <% }%>
          </div><% } catch(Exception ex){} %>
          <!--/Content-->
        </div>
      </div>
    </div>
    <%@include file='/WEB-INF/jspf/footer.jspf' %>
  </body>
</html>
