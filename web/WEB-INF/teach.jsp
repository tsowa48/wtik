<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'/>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'/>
    <title>Территориальная избирательная комиссия ${Region}</title>
  </head>
  <body class='container'>
    <%@include file='/WEB-INF/jspf/topmenu.jspf' %>
    <img src='/header.jpg?rand=<%=(new java.util.Date()).getTime() %>' width='100%' style='max-height:150px;'/>
    <div class='container-fluid'>
      <div class='row' style='margin-top: 20px;'>
        <div class='col-sm-5 col-lg-3'>
          <%@include file='/WEB-INF/jspf/sidemenu.jspf' %>
          <%@include file='/WEB-INF/jspf/banners.jspf' %>
        </div>
        <div class='col-sm-7 col-lg-9' style='padding-right:0px;'>
          <!--Content-->
          <div class='panel panel-default'>
            <div class='panel-heading' style='text-align:center'><b>Методические материалы</b></div>
            <div class='panel-body'>
              <p align='center'><b><a href='http://lipetsk.izbirkom.ru/obuchenie/'>Методические материалы избирательной комиссии Липецкой области</a></b></p>
            </div>
          </div>
          
          <div class='panel panel-default'>
            <div class='panel-heading' style='text-align:center'><b>Планы и графики обучения</b></div>
            <% if("1".equals(request.getAttribute("planned"))) { %>
            <style> th {text-align:center;}</style>
            <table class='panel-body table table-bordered'>
              <tr><th>Наименование темы</th><th>Дата и время проведения</th><th>Место проведения</th><th>Категория обучаемых</th></tr>
              <%
              String[] data = (String[])request.getAttribute("Data");
              for(String x : data) {
                String[] L = x.split(";");
                try {
                  out.print("<tr><td>".concat(L[0]).concat("</td><td><b>").concat(L[1]).concat("</b> ").concat(L[2]).concat("</td><td>").concat(L[3]).concat("</td><td>").concat(L[4]).concat("</td></tr>"));
                } catch(ArrayIndexOutOfBoundsException aiex) {}
              }
              %>
            </table>
            <% } else
                 out.print("<div class='panel-body'><p align='center'><b>Обучение не запланировано</b></p></div>");
            %>
          </div>
          <!--/Content-->
        </div>
      </div>
    </div>
    <%@include file='/WEB-INF/jspf/footer.jspf' %>
  </body>
</html>
