<%@page import="tik.news"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'/>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'/>
    <title>Территориальная избирательная комиссия ${Region}</title>
  </head>
  <body class='container'>
    <%@include file='/WEB-INF/jspf/topmenu.jspf' %>
    <img src='/header.jpg?rand=<%=(new java.util.Date()).getTime() %>' width='100%' style='max-height:150px;'/>
    <div class='container-fluid'>
      <div class='row' style='margin-top: 20px;'>
        <div class='col-sm-5 col-lg-3'>
          <%@include file='/WEB-INF/jspf/sidemenu.jspf' %>
          <%@include file='/WEB-INF/jspf/banners.jspf' %>
        </div>
        <div class='col-sm-7 col-lg-9' style='padding-right:0px;'>
          <!--Content-->
          <% if(request.getParameter("id") != null) { news N = (news)request.getAttribute("News"); %>
          <div class='panel panel-default'>
            <div class='panel-heading' style='text-align:center'><b><%=N.getHeader() %></b></div>
            <div class='panel-body'>
              <img src='/pic.jpg?id=<%=N.getID() %>' style='max-width:150px;max-height:150px;float:left;margin:5px;'/>
              <p><%=N.getContent().replace("<img ","<img style='max-width:100%;' ") %></p>
            </div>
            <div class='panel-footer'>Опубликовано: <%=N.getDate() %></div>
          </div>
          <% } else { try { List<news> N = (List<news>)request.getAttribute("NewsList"); %>
          <div class='panel-group'>
            <% for(int i = 0; i < N.size(); ++i) { %>
            <div class='panel panel-default'>
              <div class='panel-body'>
                <img src='/pic.jpg?id=<%=N.get(i).getID() %>' style='max-width:150px;max-height:150px;float:left;margin:5px;'/>
                <h4><%=N.get(i).getHeader() %></h4>
                <p align='justify'><%=N.get(i).getAnnounce() %></p>
              </div>
              <div class='panel-footer'>Опубликовано: <%=N.get(i).getDate() %> <a href='/news?id=<%=N.get(i).getID() %>' style='float:right;'>Подробнее</a></div>
            </div>
            <% } %>
          </div>
          <% } catch(Exception ex){} } %>
          <!--/Content-->
        </div>
      </div>
    </div>
    <%@include file='/WEB-INF/jspf/footer.jspf' %>
  </body>
</html>
