<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'/>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'/>
    <title>Территориальная избирательная комиссия ${Region}</title>
  </head>
  <body class='container'>
    <%@include file='/WEB-INF/jspf/topmenu.jspf' %>
    <img src='/header.jpg?rand=<%=(new java.util.Date()).getTime() %>' width='100%' style='max-height:150px;'/>
    <div class='container-fluid'>
      <div class='row' style='margin-top: 20px;'>
        <div class='col-sm-5 col-lg-3'>
          <%@include file='/WEB-INF/jspf/sidemenu.jspf' %>
          <%@include file='/WEB-INF/jspf/banners.jspf' %>
        </div>
        <div class='col-sm-7 col-lg-9' style='padding-right:0px;'>
          <!--Content-->
          <style> th {text-align:center}</style>
          <table class='table table-bordered table-striped'>
            <thead><tr><th>Название территории</th><th style='width:30%'>Численность избирателей<br>по состоянию на ${modtime}</th></tr></thead>
            <tbody>
              <% Integer Itogo = 0;
              String[] data = (String[])request.getAttribute("Data");
              for(String x : data) {
                String[] x_ = x.split(";");
                try {
                  Itogo += Integer.parseInt(x_[1]);
                  out.print("<tr><td>".concat(x_[0]).concat("</td><td>").concat(x_[1]).concat("</td></tr>"));
                } catch(ArrayIndexOutOfBoundsException aiex) {}
              }
              out.print("<tr><td><b>Итого:</b></td><td><b>".concat(String.valueOf(Itogo)).concat("</b></td></tr>"));
              %>
            </tbody>
          </table>
          <!--/Content-->
        </div>
      </div>
    </div>
    <%@include file='/WEB-INF/jspf/footer.jspf' %>
  </body>
</html>