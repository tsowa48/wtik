<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<% /*
conn = psycopg2.connect("dbname='iklo' user='iklo' password='48s000' host='localhost'")
cur.execute("select fullname, name, address from ik where name='" + str(serv) + "';") */
%>
<html>
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'/>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'/>
    <title>Территориальная избирательная комиссия ${Region}</title>
  </head>
  <body class='container'>
    <%@include file='/WEB-INF/jspf/topmenu.jspf' %>
    <img src='/header.jpg?rand=<%=(new java.util.Date()).getTime() %>' width='100%' style='max-height:150px;'/>
    <div class='container-fluid'>
      <div class='row' style='margin-top: 20px;'>
        <div class='col-sm-5 col-lg-3'>
          <%@include file='/WEB-INF/jspf/sidemenu.jspf' %>
          <%@include file='/WEB-INF/jspf/banners.jspf' %>
        </div>
        <div class='col-sm-7 col-lg-9' style='padding-right:0px;'>
          <!--Content-->
          <div class='panel panel-default'>
            <div class='panel-heading' style='text-align:center'><b>Региональные отделения политических партий</b></div>
            <div class='list-group panel-body' style='padding:0px;'>
              <a href='http://www.lipetsk.izbirkom.ru/etc/spisok_pp_lo.doc' class='list-group-item'>Список региональных отделений политических партий, зарегистрированных на территории Липецкой области</a>
              <a href='http://www.cikrf.ru/news/relevant/2012/11/13/partii.html' class='list-group-item'>Список зарегистрированных политических партий</a>
              <a href='http://www.lipetsk.izbirkom.ru/upload/iblock/471/9-108-6_%D0%9E%20%D1%81%D0%BF%D0%B8%D1%81%D0%BA%D0%B0%D1%85%20%D0%BF%D0%BE%D0%BB%D0%B8%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D1%85%20%D0%BF%D0%B0%D1%80%D1%82%D0%B8%D0%B9.doc' class='list-group-item'>О списках политических партий, выдвижение которыми (их региональными отделениями и иными структурными подразделениями) кандидатов, списка кандидатов считается поддержанным избирателями и не требуют сбора подписей на выборах депутатов Липецкого областного Совета депутатов, депутатов представительных органов муниципальных образований в Липецкой области</a>
              <a href='http://www.cikrf.ru/politparty/pol_par6.pdf' class='list-group-item'>Справочник «Политические партии в Российской Федерации 2017 год». Выпуск 6. Москва, 2017 год</a>
            </div>
          </div>
          <!--/Content-->
        </div>
      </div>
    </div>
    <%@include file='/WEB-INF/jspf/footer.jspf' %>
  </body>
</html>