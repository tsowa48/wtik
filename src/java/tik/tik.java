package tik;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;

public class tik {
  private static final String[] tikCIK = new String[] {
    "",
    "4484001129423",
    "4484002161460",
    "4484003173519",
    "9489004182194",
    "4484005156067",
    "4484006136704",
    "4484007144237",
    "4484008143049",
    "4484009156422",
    "4484010132283",
    "4484011132087",
    "4484012185517",
    "4484013140214",
    "",
    "4484015160122",
    "448401701922",
    "4484017135025",
    "4484018167189",
    "4484019126536",
    "4484020240716",
    "9489021115472",
    "4484022180855",
    "9489023129018",
    "4484024182454",
    "2482000524907"
  };
  private final static String[] tikName = new String[] {
    "",
    "Воловского района",
    "Грязинского района",
    "Данковского района",
    "Добринского района",
    "Добровского района",
    "Долгоруковского района",
    "города Ельца",
    "Елецкого района",
    "Задонского района",
    "Измалковского района",
    "Краснинского района",
    "Лебедянского района",
    "Лев-Толстовского района",
    "",
    "Липецкого района",
    "Становлянского района",
    "Тербунского района",
    "Усманского района",
    "Хлевенского района",
    "Чаплыгинского района",
    "Левобережного округа г. Липецка",
    "№1 Октябрьского округа г. Липецка",
    "Правобережного округа г. Липецка",
    "Советского округа г. Липецка",
    "№2 Октябрьского округа г. Липецка"
  };

  public static String getName(Integer num) {
    return tikName[num];
  }
  public static String getCIK(Integer num) {
    return tikCIK[num];
  }
  
  public static String getAddress(Integer num) {
    try {
      String address = (String)Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/address.txt")).toArray()[0];
      return address;
    } catch(IOException ex) { return ""; }
  }
  public static void setAddress(String address, Integer num) {
    try {
      PrintWriter writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/address.txt");
      writer.print(new String(address.getBytes("ISO-8859-1")));
      writer.close();
    } catch(IOException iex) { }
  }
  
  public static String getPredFIO(Integer num) {
    try {
      String pred = new String(Files.readAllBytes(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/pred.txt")));
      return pred;
    } catch(IOException ex) { return ""; }
  }
  public static void setPredFIO(String fio, Integer num) {
    try {
      PrintWriter writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/pred.txt");
      writer.print(new String(fio.getBytes("ISO-8859-1")));
      writer.close();
    } catch(IOException iex) { }
  }
  
  public static String getObrash(Integer num) {
    try {
      String obrash = String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/obrash.txt")));
      return obrash;
    } catch(IOException ex) { return ""; }
  }
  public static void setObrash(String obrash, Integer num) {
    try {
      PrintWriter writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/obrash.txt");
      writer.print(new String(obrash.getBytes("ISO-8859-1")));
      writer.close();
    } catch(IOException iex) { }
  }
  
  public static String[] getInfo(Integer num) {
    try {
      String[] info = Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/info.txt")).toArray(new String[0]);
      return info;
    } catch(IOException ex) { return new String[] { "нет" }; }
  }
  public static void setInfo(String info, Integer num) {
    try {
      PrintWriter writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/info.txt");
      writer.print(new String(info.getBytes("ISO-8859-1")));
      writer.close();
    } catch(IOException iex) { }
  }
  
  public static String getGrafik(Integer num) {
    try {
      String grafik = String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/grafik.txt")));
      return grafik;
    } catch(IOException ex) { return ""; }
  }
  public static void setGrafik(String grafik, Integer num) {
    try {
      PrintWriter writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/grafik.txt");
      writer.print(new String(grafik.getBytes("ISO-8859-1")));
      writer.close();
    } catch(IOException iex) { }
  }
  
  public static List<String> getDocs(Integer num) {
    try {
      File[] docs = new File("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/docs/").listFiles();
      List<String> docNames = Arrays.asList(docs).stream().filter(x -> x.isFile()).map(d -> d.getName()).collect(Collectors.toList());
      List<String> laws = docNames.stream().filter(x -> x.startsWith("№")).sorted(Comparator.comparingInt(x -> Integer.parseInt(x.split("_")[1].split(" ")[0].contains("-") ? x.split("_")[1].split(" ")[0].split("-")[0] : x.split("_")[1].split(" ")[0]))).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
      List<String> other = docNames.stream().filter(x -> !x.startsWith("№")).collect(Collectors.toList());
      laws.addAll(other);
      return laws;//docNames;
    } catch(NullPointerException ex) { return null; }
  }
  
  public static List<Integer> getNewsIDs(Integer num) {
    try {
    File[] News = new File("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/").listFiles();
    List<Integer> newsNames = Arrays.asList(News).stream().filter(x -> !x.isFile()).map(d -> Integer.valueOf(d.getName())).collect(Collectors.toList());
    return newsNames;
    } catch(NullPointerException ex) { return null; }
  }
  public static List<news> getNews(Integer num) {
    try {
    File[] News = new File("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/").listFiles();
    List<news> newsNames = Arrays.asList(News).stream().filter(x-> x.isDirectory()).sorted((x,y) -> Integer.compare(Integer.parseInt(x.getName()), Integer.parseInt(y.getName()))).map(x-> {
      try {
        return new news(Integer.parseInt(x.getName()),
                String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(x.getName()).concat("/name.txt")))),
                String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(x.getName()).concat("/announce.txt")))),
                String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(x.getName()).concat("/content.txt")))),
                String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(x.getName()).concat("/pubdate.txt")))));
      }catch(IOException ex){return null;}
    }).collect(Collectors.toList());
    Collections.reverse(newsNames);
    return newsNames;
    } catch(NullPointerException ex) {return null;}
  }
  public static news getNewsById(Integer num, Integer id) {
    try {
    String name = String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(String.valueOf(id)).concat("/name.txt"))));
    String announce = String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(String.valueOf(id)).concat("/announce.txt"))));
    String content = String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(String.valueOf(id)).concat("/content.txt"))));
    String pubDate = String.join("\n", Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/".concat(String.valueOf(id)).concat("/pubdate.txt"))));
    return new news(id, name, announce, content, pubDate);
    } catch(Exception ex) { return null; }
  }
  public static void setNews(Integer num, Integer ID, String header, String announce, String content, String pubDate) {
    try {
      PrintWriter writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/" + String.valueOf(ID) + "/name.txt");
      writer.print(new String(header.getBytes("ISO-8859-1")));
      writer.close();
      
      writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/" + String.valueOf(ID) + "/announce.txt");
      writer.print(new String(announce.getBytes("ISO-8859-1")));
      writer.close();
      
      writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/" + String.valueOf(ID) + "/content.txt");
      writer.print(new String(content.getBytes("ISO-8859-1")));
      writer.close();
      
      writer = new PrintWriter("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/" + String.valueOf(ID) + "/pubdate.txt");
      writer.print(new String(pubDate.getBytes("ISO-8859-1")));
      writer.close();
    } catch(IOException iex) { }
  }
  
  public static void removeNews(Integer num, Integer ID) {
    deleteDirectory(new File("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/" + String.valueOf(ID)));
  }
  public static Integer getMaxNewsID(Integer num) {
    File[] News = new File("/var/www/tix/WEB-INF/data/" + String.valueOf(num) + "/news/").listFiles();
    Optional<Integer> ret = Arrays.asList(News).stream().filter(x-> x.isDirectory()).map(x-> Integer.parseInt(x.getName())).max((x,y) -> Integer.compare(x, y));
    if(ret.isPresent())
      return ret.get();
    else return -1;
  }
  
  
  private static boolean deleteDirectory(File directory) {
    if(directory.exists()){
      File[] files = directory.listFiles();
      if(null != files) {
        for(int i = 0; i < files.length; ++i) {
          if(files[i].isDirectory())
            deleteDirectory(files[i]);
          else
            files[i].delete();
        }
      }
    }
    return directory.delete();
  }
}
