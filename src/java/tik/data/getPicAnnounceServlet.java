package tik.data;

import java.io.IOException;
import java.nio.file.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class getPicAnnounceServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    String newsId = request.getParameter("id");
    if(newsId != null && !"".equals(newsId)) {
      response.setContentType("image/jpeg");
      byte[] buf = Files.readAllBytes(Paths.get("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/news/").concat(newsId).concat("/pic.jpg")));
      response.getOutputStream().write(buf);
    }
  }

}
