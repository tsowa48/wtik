package tik.data;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class getDocServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    Integer id = Integer.parseInt(request.getParameter("id"));
    String fileName = tik.tik.getDocs(tikNum).get(id);
    Path F = Paths.get("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/docs/").concat(fileName));
    response.setContentType(Files.probeContentType(F));
    fileName = URLEncoder.encode(fileName, "UTF-8");
    response.setHeader("Content-Disposition", "attachment; filename=\"".concat(fileName).concat("\""));
    byte[] buf = Files.readAllBytes(F);
    response.setContentLength(buf.length);
    response.getOutputStream().write(buf);
  }

}
