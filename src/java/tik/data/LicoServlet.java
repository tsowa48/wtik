package tik.data;

import java.io.*;
import java.nio.file.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class LicoServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("image/jpeg");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    byte[] buf = Files.readAllBytes(Paths.get("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/lico.jpg")));
    response.getOutputStream().write(buf);
  }
}
