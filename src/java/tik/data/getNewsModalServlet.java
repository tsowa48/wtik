package tik.data;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import tik.news;

public class getNewsModalServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    String nid = request.getParameter("nid");
    response.setContentType("text/html;charset=cp1251");
    response.setCharacterEncoding("cp1251");
    if(nid != null) {
      news N = tik.tik.getNewsById(tikNum, Integer.valueOf(nid));
      PrintWriter pw = response.getWriter();

      pw.println("<div class='input-group'>");
      pw.println("<span class='input-group-addon'>Название:</span>");
      pw.println("<input class='form-control' type='text' name='newsHeader' required value='" + N.getHeader() + "'/>");
      pw.println("</div><br>");
      pw.println("<div class='input-group'>");
      pw.println("<span class='input-group-addon'>Анонс:</span>");
      pw.println("<input class='form-control' type='text' name='newsAnnounce' required value='" + N.getAnnounce() + "'/>");
      pw.println("</div><br>");
      pw.println("<div class='input-group'>");
      pw.println("<span class='input-group-addon'>Картинка анонса:</span>");
      pw.println("<input class='form-control' type='file' name='newsImage' accept='.jpg'/>");
      pw.println("</div><br>");
      pw.println("<div class='item-group'>");
      pw.println("<label for='newsContent1'>Содержание:</label>");
      pw.println("<textarea class='form-control' rows='20' id='newsContent1' name='newsContent' required>" + N.getContent() + "</textarea>");
      pw.println("<script>CKEDITOR.replace('newsContent1', {\n" +
"  toolbarGroups: [\n" +
"    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },\n" +
"		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },\n" +
"		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },\n" +
"		{ name: 'forms', groups: [ 'forms' ] },\n" +
"		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },\n" +
"		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },\n" +
"		{ name: 'links', groups: [ 'links' ] },\n" +
"		{ name: 'insert', groups: [ 'insert' ] },\n" +
"		'/',\n" +
"		{ name: 'styles', groups: [ 'styles' ] },\n" +
"		{ name: 'colors', groups: [ 'colors' ] },\n" +
"		{ name: 'tools', groups: [ 'tools' ] },\n" +
"		{ name: 'others', groups: [ 'others' ] },\n" +
"		{ name: 'about', groups: [ 'about' ] }\n" +
"			],\n" +
"      removeButtons: 'Source,Save,NewPage,Preview,Print,Templates,Find,Replace,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,Blockquote,BidiRtl,BidiLtr,Language,Anchor,Flash,Smiley,SpecialChar,PageBreak,Iframe,About,ShowBlocks,Maximize,Styles,Format,Redo,Undo',\n" +
"      removePlugins: 'elementspath'\n" +
"});</script>");
      pw.println("</div>");
    }
  }
}
