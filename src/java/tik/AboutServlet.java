package tik;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class AboutServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/about.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    request.setAttribute("Region", tik.getName(tikNum));
    request.setAttribute("Address", tik.getAddress(tikNum));
    request.setAttribute("CIK", tik.getCIK(tikNum));
    String html = http.get("http://www.lipetsk.vybory.izbirkom.ru/region/lipetsk", "action=ik&vrn=".concat(tik.getCIK(tikNum)));
    html = html.substring(html.indexOf("<table cellpadding=\"0\" cellspacing=\"0\">"), html.indexOf("</table>", html.indexOf("<table cellpadding=\"0\" cellspacing=\"0\">"))).concat("</table>");
    html = html.replace("cellpadding=\"0\" cellspacing=\"0\"", "class='table table-bordered table-striped'");
    //html = html[html.find("<table") : html.find("</table>")] + "</table>"
    request.setAttribute("tblTIKContent", html);
    reqDispatcher.forward(request,response);
  }
}
