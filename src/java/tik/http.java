package tik;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import javax.servlet.http.Part;
//import javax.net.ssl.HttpsURLConnection;

public class http {
  private static String GET_POST(String method, String url, String data) {
    try {
      URL obj = new URL(url.concat("get".equals(method.toLowerCase()) ? "?".concat(data) : ""));
      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
  		con.setRequestMethod(method.toUpperCase());
      con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
      if(!"get".equals(method.toLowerCase())) {
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        con.setRequestProperty("Content-Length", String.valueOf(data.length()));
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();
      }// else {
      //con.setRequestProperty("Content-Type", "text/html;charset=windows-1251");
      //}
  		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), Charset.forName("windows-1251")));
      String inputLine;
      StringBuilder response = new StringBuilder();
  		while ((inputLine = in.readLine()) != null)
  			response.append(inputLine);
    	in.close();
      return response.toString();
    } catch(IOException ex) {
      return ex.toString();
    }
  }
  public static String post(String url, String data) {
    return GET_POST("post", url, data);
	}
  public static String get(String url, String data) {
    return GET_POST("get", url, data);
	}
  
  public static void saveFile(Part file, String writeTo) {
    int fileSize = (int)file.getSize();
    if(fileSize > 0) {
      try {
        InputStream fileContent = file.getInputStream();
        byte[] buf = new byte[fileSize];
        fileContent.read(buf);
        fileContent.close();
        FileOutputStream fos = new FileOutputStream(writeTo, false);
        fos.write(buf);
        fos.close();
      }catch(IOException iex){}
    }
  }
}
