package tik;

import java.io.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class NewsServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/news.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    
    request.setAttribute("Region", tik.getName(tikNum));
    request.setAttribute("Pred_FIO", tik.getPredFIO(tikNum));
    request.setAttribute("Pred_Welcome", "<p>".concat(tik.getObrash(tikNum).replace("\n\n", "</p><p>").replace("\n", "<br>")).concat("</p>"));
    request.setAttribute("Address", tik.getAddress(tikNum));
    String id = request.getParameter("id");
    if(id != null && !"".equals(id))
      request.setAttribute("News", tik.getNewsById(tikNum, Integer.parseInt(id)));
    else
      request.setAttribute("NewsList", tik.getNews(tikNum));
    
    reqDispatcher.forward(request, response);
  }

  
}
