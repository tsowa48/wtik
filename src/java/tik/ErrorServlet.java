package tik;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class ErrorServlet extends HttpServlet {

  protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
    response.setContentType("text/html;charset=UTF-8");
    //Throwable throwable = (Throwable)
      //request.getAttribute("javax.servlet.error.exception");
      Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
      //String servletName = (String)
      //request.getAttribute("javax.servlet.error.servlet_name");
      //String requestUri = (String)
      //request.getAttribute("javax.servlet.error.request_uri");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    request.setAttribute("Region", tik.getName(tikNum));
    request.setAttribute("Address", tik.getAddress(tikNum));
    request.setAttribute("Error", statusCode);
    reqDispatcher.forward(request,response);
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    processRequest(request, response);
  }
}
