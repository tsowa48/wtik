package tik;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.http.*;

public class TeachServlet extends HttpServlet {
  
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/teach.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    request.setAttribute("Region", tik.getName(tikNum));
    request.setAttribute("Address", tik.getAddress(tikNum));
    
    String[] Data = new String[] {};
    String planned = "0";
    try {
      Data = Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/teach.csv")), Charset.forName("cp1251")).toArray(new String[0]);
      planned = "1";
    } catch(Exception ex) {}
    request.setAttribute("planned", planned);
    request.setAttribute("Data", Data);
    
    reqDispatcher.forward(request,response);
  }
}
