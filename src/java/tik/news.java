package tik;

public class news {
  private String Header;
  private String Announce;
  private String Date;
  private Integer ID;
  private String Content;

  public news(Integer id, String header, String announce, String content, String pubDate) {
    this.ID = id;
    this.Header = header;
    this.Announce = announce;
    this.Content = content;
    this.Date = pubDate;
  }
  
  public String getHeader() {
    return Header;
  }

  public void setHeader(String Header) {
    this.Header = Header;
  }

  public String getAnnounce() {
    return Announce;
  }

  public void setAnnounce(String Announce) {
    this.Announce = Announce;
  }

  public String getDate() {
    return this.Date;
  }

  public void setDate(String pubDate) {
    this.Date = pubDate;
  }

  public Integer getID() {
    return ID;
  }

  public void setID(Integer ID) {
    this.ID = ID;
  }
  public String getContent() {
    return Content;
  }

  public void setContent(String Content) {
    this.Content = Content;
  }
}
