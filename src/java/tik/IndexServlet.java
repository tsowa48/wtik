package tik;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class IndexServlet extends HttpServlet {
    
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/index.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    request.setAttribute("Region", tik.getName(tikNum));
    request.setAttribute("Pred_FIO", tik.getPredFIO(tikNum));
    request.setAttribute("Pred_Welcome", "<p>".concat(tik.getObrash(tikNum).replace("\n\n", "</p><p>").replace("\n", "<br>")).concat("</p>"));
    request.setAttribute("Address", tik.getAddress(tikNum));
    request.setAttribute("NewsList", tik.getNews(tikNum));
    reqDispatcher.forward(request,response);
  }
}
