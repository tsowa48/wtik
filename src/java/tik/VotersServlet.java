package tik;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.http.*;

public class VotersServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/voters.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    request.setAttribute("Region", tik.getName(tikNum));
    request.setAttribute("Address", tik.getAddress(tikNum));
    
    String modtime = " ";
    String[] Data = new String[] {};
    try {
      Data = Files.readAllLines(Paths.get("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/voters.csv")), Charset.forName("cp1251")).toArray(new String[0]);
      String[] modTime = (new SimpleDateFormat("dd.MM.yyyy").format(new Date(new File("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/voters.csv")).lastModified()))).split("[.]");
      if((Integer.parseInt(modTime[0]) > 1 && Integer.parseInt(modTime[1]) > 1) && Integer.parseInt(modTime[1]) < 7)
        modtime = "01.01." + modTime[2];
      else
        modtime = "01.07." + modTime[2];
    } catch(Exception ex) {}
    request.setAttribute("modtime", modtime);
    request.setAttribute("Data", Data);
    reqDispatcher.forward(request,response);
  }
}
