package tik;

import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;

@MultipartConfig
public class ManagerServlet extends HttpServlet {
  //request.getRemoteUser()
  private void initAttributes(HttpServletRequest request, Integer nTik) {
    request.setAttribute("Region", tik.getName(nTik));
    request.setAttribute("Pred", tik.getPredFIO(nTik));//from File pred.txt
    request.setAttribute("Address", tik.getAddress(nTik));//from file address.txt
    String[] info = tik.getInfo(nTik);//from file info.txt
    request.setAttribute("Phone", info.length > 0 ? info[0] : "нет");
    request.setAttribute("Fax", info.length > 1 ? info[1] : "нет");
    request.setAttribute("Email", info.length > 2 ? info[2] : "");
    request.setAttribute("Obrash", tik.getObrash(nTik));//from file obrash.txt
    request.setAttribute("Grafik", tik.getGrafik(nTik));//from file grafik.txt
    request.setAttribute("DocsList", tik.getDocs(nTik));//from dir /docs/
    request.setAttribute("NewsList", tik.getNews(nTik));//from dir /news/
  }
  
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    //Date d = new Date();
    //d.setTime(d.getTime() + 60);
    //Thu, 01 Jan 1970 03:00:00 MSK
    //response.addHeader("Expires", (new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z")).format(d));
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/manager.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    
    String action = request.getParameter("action");
    if("delDoc".equals(action)) {
      String did = request.getParameter("id");
      if(did != null)
        if(Integer.parseInt(did) > -1)
          Files.delete(Paths.get("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/docs/").concat(tik.getDocs(tikNum).get(Integer.parseInt(did)))));
    }
    initAttributes(request, tikNum);
    reqDispatcher.forward(request, response);
  }
  
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/manager.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));

    String action = request.getParameter("action");
    
    if("index".equals(action)) {
      String fio = request.getParameter("pred");
      String obrash = request.getParameter("obrash");
      tik.setPredFIO(fio, tikNum);
      tik.setObrash(obrash, tikNum);
      Part header = request.getPart("header");
      http.saveFile(header, "/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/header.jpg"));
      Part lico = request.getPart("lico");
      http.saveFile(lico, "/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/lico.jpg"));
    } else if("voters".equals(action)) {
      Part voters = request.getPart("voters");
      http.saveFile(voters, "/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/voters.csv"));
    } else if("teach".equals(action)) {
      Part voters = request.getPart("teach");
      http.saveFile(voters, "/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/teach.csv"));
    } else if("reception".equals(action)) {
      String address = request.getParameter("address");
      String phone = request.getParameter("phone");
      String fax = request.getParameter("fax");
      String email = request.getParameter("email");
      String grafik = request.getParameter("grafik");
      tik.setAddress(address, tikNum);
      String info = phone.concat("\n").concat(fax).concat("\n").concat(email);
      tik.setInfo(info, tikNum);
      tik.setGrafik(grafik, tikNum);
    } else if("adddocs".equals(action)) {
      Part doc = request.getPart("doc");
      http.saveFile(doc, "/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/docs/").concat(doc.getSubmittedFileName()));
    } else if("addnews".equals(action)) {
      String newsHeader = request.getParameter("newsHeader");
      String newsAnnounce = request.getParameter("newsAnnounce");
      String newsContent = request.getParameter("newsContent");
      String pubDate = (new SimpleDateFormat("dd.MM.yyyy")).format(new Date());
      Part newsImage = request.getPart("newsImage");
      Integer maxID = tik.getMaxNewsID(tikNum) + 1;
      (new File("/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/news/").concat(String.valueOf(maxID)))).mkdir();
      http.saveFile(newsImage, "/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/news/").concat(String.valueOf(maxID)).concat("/pic.jpg"));
      tik.setNews(tikNum, maxID, newsHeader, newsAnnounce, newsContent, pubDate);
    } else if("editnews".equals(action)) {
      String newsHeader = request.getParameter("newsHeader");
      String newsAnnounce = request.getParameter("newsAnnounce");
      String newsContent = request.getParameter("newsContent");
      Integer newsID = Integer.valueOf(request.getParameter("news_id"));
      Part newsImage = request.getPart("newsImage");
      http.saveFile(newsImage, "/var/www/tix/WEB-INF/data/".concat(String.valueOf(tikNum)).concat("/news/").concat(String.valueOf(newsID)).concat("/pic.jpg"));
      news N = tik.getNewsById(tikNum, newsID);
      tik.setNews(tikNum, newsID, newsHeader, newsAnnounce, newsContent, N.getDate());
    }
    initAttributes(request, tikNum);
    reqDispatcher.forward(request,response);
  }
}
