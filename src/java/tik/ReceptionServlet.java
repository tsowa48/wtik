package tik;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class ReceptionServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/reception.jsp");
    String server = request.getServerName();
    Integer tikNum = Integer.parseInt(server.substring(server.indexOf("tik") + 3, server.indexOf(".")));
    request.setAttribute("Region", tik.getName(tikNum));
    request.setAttribute("Address", tik.getAddress(tikNum));
    String[] info = tik.getInfo(tikNum);
    request.setAttribute("Phone", info.length > 0 ? info[0] : "нет");
    request.setAttribute("Fax", info.length > 1 ? info[1] : "нет");
    request.setAttribute("Email", info.length > 2 ? info[2] : "");
    request.setAttribute("Grafik", tik.getGrafik(tikNum).replace("\n", "<br>"));
    
    reqDispatcher.forward(request,response);
  }

}
